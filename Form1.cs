﻿using Notepad_.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad_
{
    // Я постарался реализовать весь обязательный функционал и первые 3 пункта дополнительного.
    // (журналирование версий, открытие файлов cs и ctrl+z и ctrl+shift+z).
    // В программе есть кнопка помощи.

    public partial class Notepad : Form
    {
        // Динамически меняется, активное поле текста. 
        private RichTextBox activeTextBox
        {
            get
            {
                return (RichTextBox)tabControl.SelectedTab.Controls[0];
            }
        }

        private ColorScheme activeColorScheme;
        // Выбранная цветовая тема.
        private ColorScheme ActiveColorScheme
        {
            get => activeColorScheme;
            set
            {
                try
                {
                    activeColorScheme = value;
                    tableLayoutPanel1.BackColor = activeColorScheme.mainColor;
                    this.BackColor = activeColorScheme.mainColor;

                    foreach (TabPage tab in MenuControl.TabPages)
                    {
                        tab.BackColor = activeColorScheme.mainColor;
                        tab.ForeColor = activeColorScheme.textColor;
                    }
                    foreach (TabPage tab in tabControl.TabPages)
                    {
                        tab.BackColor = activeColorScheme.mainColor;
                        // RichTextBox полностью не закрашивается (остаются рамки), поэтому не закрашиваю его.
                        //((RichTextBox)(tab.Controls[0])).BackColor = activeColorScheme.mainColor;
                        var currentTextBox = (RichTextBox)(tab.Controls[0]);
                        currentTextBox.SelectAll();
                        currentTextBox.SelectionColor = activeColorScheme.textColor;
                        currentTextBox.DeselectAll();
                        currentTextBox.ForeColor = activeColorScheme.textColor;
                    }
                }
                catch { }
            }
        }

        
        // Поле, отвечающее за названия новых документов.
        private int documentsCount = 0;

        private Font activeFont = new Font("Segoe UI", 9);

        // Словарь, по экземпляру текстбокса получить полный путь к файлу.
        private Dictionary<RichTextBox, string> tabsFileNames = new Dictionary<RichTextBox, string>();
        // Фильтр, файлы каких форматов доступны для открытия и сохранения.
        private readonly string filterFiles = "Текстовые файлы |*.txt;*.rtf|" +
                                              "Программный код |*.cs";
        // Автосохранение.
        Timer autoSaveTimer = new Timer(); 
        // Журналирование версий.
        Timer saveHistoryTimer = new Timer();

        bool formLoaded = false;
        // Путь к сохранению истории версий.
        string historyPath = "history";


        // Загружаем выбранные настройки до перезапуска (если они были сохранены)
        public void LoadSettings()
        {
            try
            {
                colorSchemeSwitcher.SelectedItem = ColorScheme.GetColorSchemeNameById(Settings.Default.ColorSchemeIndex);
                AutoSavePeriodChanger.Value = Settings.Default.AutoSavePeriod;
                SaveHistoryIntervalChanger.Value = Settings.Default.SaveHistoryInterval;
                if (Settings.Default.OpenedDocuments != null)
                {
                    var nonExistentFiles = new List<string>();
                    foreach (string fileName in Settings.Default.OpenedDocuments)
                    {
                        try
                        {
                            OpenNewTabButton_Click(this, new StringEventArgs { FileName = fileName });
                        }
                        catch
                        {
                            MessageBox.Show($"Не удается получить доступ к файлу {fileName}, открытому в прошлой сессии", "Ошибка",
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            nonExistentFiles.Add(fileName);
                        }
                    }
                    foreach (string fileName in nonExistentFiles)
                    {
                        Settings.Default.OpenedDocuments.Remove(fileName);
                    }
                }
                colorSchemeSwitcher.SelectedItem = ColorScheme.GetColorSchemeNameById(Settings.Default.ColorSchemeIndex);
            }
            catch { }
            
        }

        public Notepad()
        {
            InitializeComponent();
            formatControl.StyleChangedEvent += new EventHandler(TextStyle_Changed);

            LoadSettings();

            MenuControl.SelectedIndex = 0;
            documentsCount = 1;
            autoSaveTimer.Tick += AutoSaveTimer_Tick;
            saveHistoryTimer.Tick += SaveHistoryTimer_Tick;
          
            // Кнопки форматирования.
            CopyButton.Click += (s, e) => activeTextBox.Copy();
            CutButton.Click += (s, e) => activeTextBox.Cut();
            PasteButton.Click += (s, e) => activeTextBox.Paste();
            SelectAllButton.Click += (s, e) => activeTextBox.SelectAll();
            CancelButton.Click += (s, e) => activeTextBox.Undo();
            RepeatButton.Click += (s, e) => activeTextBox.Redo();

            // Создаем скрытую папку для журналирования (если ее еще нет).
            if (!Directory.Exists(historyPath))
            {
                DirectoryInfo directory = Directory.CreateDirectory(historyPath);
                directory.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }

            formLoaded = true;
        }

        // Обработчик события срабатываения таймера журналирования.
        private void SaveHistoryTimer_Tick(object sender, EventArgs e)
        {
            // Может быть эксепшн, если форма закрылась в момент срабатывания.
            try
            {
                foreach (TabPage tab in tabControl.TabPages)
                {
                    RichTextBox savingBox = (RichTextBox)(tab.Controls[0]);
                    // Если файл не сохранялся, не журналируем.
                    if (!tabsFileNames.ContainsKey(savingBox)) continue;

                    string fileName = tabsFileNames[savingBox].Split('\\')[^1].Split('.')[^2] + " "// имя файла
                                     + DateTime.Now.ToString().Replace(".", "").Replace(":", "") + "." //дата и время
                                     + tabsFileNames[savingBox].Split('\\')[^1].Split('.')[^1]; //расширение.
                    fileName = historyPath + "/" + fileName;
                    SaveFile(tab, fileName);
                }
            }
            catch { }
            
        }

        // Обработчик события срабатываения таймера автосохранения.
        // Также вызывается для сохранения всех файлов
        private void AutoSaveTimer_Tick(object sender, EventArgs e)
        {
            // Может быть эксепшн, если форма закрылась в момент срабатывания.
            try
            {
                foreach (TabPage tab in tabControl.TabPages)
                {
                    RichTextBox savingBox = (RichTextBox)(tab.Controls[0]);
                    // Если файл до этого был сохранен.
                    if (tabsFileNames.ContainsKey(savingBox))
                    {
                        SaveFile(tab, tabsFileNames[savingBox]);
                    }
                }
            }
            catch { }
        }

        // Обработчик события изменения стиля.
        private void TextStyle_Changed(object sender, EventArgs e)
        {
            // У выделентя меняем шрифт.
            activeFont = new Font(activeTextBox.Font.FontFamily, activeTextBox.Font.Size,
                (formatControl.boldControl.Checked ? FontStyle.Bold : FontStyle.Regular) |
                (formatControl.italicControl.Checked ? FontStyle.Italic : FontStyle.Regular) |
                (formatControl.underlineControl.Checked ? FontStyle.Underline : FontStyle.Regular) |
                (formatControl.strikeoutControl.Checked ? FontStyle.Strikeout : FontStyle.Regular));
            activeTextBox.SelectionFont = activeFont;
        }

        // Обработчик события изменения текста, нужно добавить 
        // К отображаемуму имени файла * (несохраненные изменения).
        private void RichTextBox_TextChanged(object sender, EventArgs e)
        {
            activeTextBox.SelectionFont = activeFont;
            if (!tabControl.SelectedTab.Text.Contains('*') && formLoaded)
            {
                tabControl.SelectedTab.Text += "*";
            }
        }
        
        // Сохраняет вкладку Tab по пути fileName.
        private void SaveFile(TabPage tab, string fileName)
        {
            try
            {
                ((RichTextBox)(tab.Controls[0])).SaveFile(fileName,
                        fileName.Substring(fileName.Length - 4, 4) == ".rtf" ?
                        RichTextBoxStreamType.RichText
                        : RichTextBoxStreamType.PlainText);
                tab.Text = tab.Text.Trim('*');
            }
            catch
            {
                MessageBox.Show($"Невозможно сохранить файл {fileName}, нет доступа для записи", "Ошибка",
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        // Событие нажатия на кнопку сохранить, также вызвается, при нажатии горячих клавиш,
        // Или в других случаях, когда нужно сохранить файл.
        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (tabsFileNames.ContainsKey(activeTextBox))
            {
                string fileName = tabsFileNames[activeTextBox];
                SaveFile(tabControl.SelectedTab, fileName);
            }
            // Файл еще не был сохранен ни по какому пути.
            else
            {
                SaveAsButton_Click(sender, e);
            }
        }
        
        // Обработчик события нажатия на кнопку сохранить как (по новому пути),
        // или если файл не был сохранен до этого)
        private void SaveAsButton_Click(object sender, EventArgs e)
        {
            string fileName = tabControl.TabPages[tabControl.SelectedIndex].Text;

            using SaveFileDialog saveDialog = new SaveFileDialog
            {
                Filter = filterFiles,
                FileName = fileName.Replace("*", ""),
                Title = "Сохранить как"
            };

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = saveDialog.FileName;
                activeTextBox.SaveFile(fileName,
                    fileName.Substring(fileName.Length - 4, 4) == ".rtf" ?
                    RichTextBoxStreamType.RichText
                    : RichTextBoxStreamType.PlainText);
                tabsFileNames[activeTextBox] = fileName;
                var fileNameList = fileName.Split('\\');
                tabControl.SelectedTab.Text = fileNameList[^1];
                Settings.Default.OpenedDocuments.Add(fileName);
            }
        }

        // Оюработчик события нажатия на кнопку открытия файла.
        // Вызывется также при запуске формы, для открытия вкладок из прошлого сеанса.
        private void OpenNewTabButton_Click(object sender, EventArgs e)
        {
            using OpenFileDialog openDialog = new OpenFileDialog
            {
                Filter = filterFiles,
                Title = "Открыть"
            };

            if (sender is Notepad || openDialog.ShowDialog() == DialogResult.OK)
            {
                CreateNewFileButton_Click(sender, e);
                string fileName = (sender is Notepad) ?
                    ((StringEventArgs)e).FileName : openDialog.FileName;

                if (Settings.Default.OpenedDocuments == null)
                {
                    Settings.Default.OpenedDocuments = new System.Collections.Specialized.StringCollection();
                }

                if (!(sender is Notepad))
                {

                    // Сохраяем в Settings путь, если открываем новую вкладку,
                    // а не загружаем форму
                    Settings.Default.OpenedDocuments.Add(fileName);
                }
                try
                {
                    if (fileName.Substring(fileName.Length - 4, 4) == ".rtf")
                    {
                        activeTextBox.LoadFile(fileName);
                    }
                    else // .cs или .txt
                    {
                        activeTextBox.Text = File.ReadAllText(fileName);
                    }
                    tabsFileNames[activeTextBox] = fileName;
                    tabControl.SelectedTab.Text = fileName.Split('\\')[^1];
                    ActiveColorScheme = activeColorScheme;
                }
                catch
                {
                    MessageBox.Show("Не удается получить доступ к файлу, возможно он открыт в другой программе", "Ошибка",
                                     MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Нужно закрыть новую вкладку.
                    CloseCurrentTabButton_Click(sender, e);
                }
                

            }
        }
        
        // Обработчик события нажатия на кнопку создания нового файла.
        // Также вызывется при открытии существуещего документа.
        private void CreateNewFileButton_Click(object sender, EventArgs e)
        {
            // Новая вкладка в TabControl.
            var newPage = new TabPage
            {
                Text = "Документ" + (++documentsCount) + ".rtf"
            };
            // TextBox на этой вкладке.
            var textBox = new RichTextBox
            {
                Dock = DockStyle.Fill,
                HideSelection = false,
                BorderStyle = BorderStyle.None,
                ForeColor = activeColorScheme.textColor
            };

            var contextMenuStrip = new ContextMenuStrip();

            var copyMenuItem = new ToolStripMenuItem("Копировать");
            var cutMenuItem = new ToolStripMenuItem("Вырезать");
            var pasteMenuItem = new ToolStripMenuItem("Втавить");
            var selectAllMenuItem = new ToolStripMenuItem("Выбрать весь текст");
            var formatMenuItem = new ToolStripMenuItem("Открыть меню форматирования");

            contextMenuStrip.Items.AddRange(new[] { copyMenuItem, cutMenuItem, pasteMenuItem,
                                                    selectAllMenuItem, formatMenuItem});
            textBox.ContextMenuStrip = contextMenuStrip;

            copyMenuItem.Click += (s, e) => activeTextBox.Copy();
            cutMenuItem.Click += (s, e) => activeTextBox.Cut();
            pasteMenuItem.Click += (s, e) => activeTextBox.Paste();
            selectAllMenuItem.Click += (s, e) => activeTextBox.SelectAll();
            formatMenuItem.Click += (s, e) => MenuControl.SelectedIndex = 2;


            newPage.Controls.Add(textBox);
            tabControl.TabPages.Add(newPage);
            tabControl.SelectedIndex = tabControl.TabCount - 1;
            // Для изменения стилей.
            activeTextBox.TextChanged += RichTextBox_TextChanged;
        }
        
        // Вызывается при нажатии соотв. кнопки.
        private void CreateNewFileNewWindowButton_Click(object sender, EventArgs e)
        {
            // Создаем новое окно и новый документ.
            Notepad newNotepad = new Notepad();
            newNotepad.Show();
            newNotepad.CreateNewFileButton_Click(sender, e);
        }

        // Обработчик события нажатия на кнопку закрытия текущей вкладки.
        // Также вызывается при закрытии формы
        private void CloseCurrentTabButton_Click(object sender, EventArgs e)
        {
            // Если вкладка содержит несохраненные изменения.
            if (tabControl.SelectedTab.Text.Contains('*'))
            {
                DialogResult result = MessageBox.Show($"Файл {tabControl.SelectedTab.Text.Trim('*')}" +
                      $" содержит несохраненные изменения. " +
                      "Сохранить внесенные корректировки?", "Предупреждение",
                      MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    SaveButton_Click(sender, e);
                }
                // Нажали отмену.
                else if (result == DialogResult.Cancel && (FormClosingEventArgs)e != null)
                {
                    ((FormClosingEventArgs)e).Cancel = true;
                    return;
                }
                else if (result != DialogResult.No)
                {
                    return;
                }
            }
            // Если вызывается не при закрытии формы.
            if (sender is Button)
            {
                try
                {
                    Settings.Default.OpenedDocuments.Remove(tabsFileNames[activeTextBox]);
                }
                catch { }
                tabControl.TabPages.Remove(tabControl.SelectedTab);
                try
                {
                    tabControl.SelectedTab = tabControl.TabPages[^1];
                }
                catch
                {
                    tabControl.SelectedTab = null;
                }
            }
        }

        // Обработчик события нажатия на кнопку помощи, показывает MessageBox.
        private void HelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Горячие клавши:" + Environment.NewLine +
                $"CTRL+S - сохранить текущий файл" + Environment.NewLine +
                $"CTRL+SHIFT+S - сохранить все файлы" + Environment.NewLine +
                $"CTRL+N - создать новый документ" + Environment.NewLine +
                $"CTRL+SHIFT+N - создать новый документ в новом окне" + Environment.NewLine +
                $"ALT+F4 - закрыть окно" + Environment.NewLine +
                $"CTRL+Z - назад" + Environment.NewLine +
                $"CTRL+SHIFT+Z - вперед" + Environment.NewLine +

                $"История версий файлов хранится в скрытой папке history, " +
                $"которая лежит в одной директории с запускаемом exe", "Помощь",
                MessageBoxButtons.OK, MessageBoxIcon.Question);
        }
        
        // Обработчик события изменения темы.
        private void colorSchemeSwitcher_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (colorSchemeSwitcher.SelectedItem == ColorScheme.colorSchemeStandart.name)
            {
                ActiveColorScheme = ColorScheme.colorSchemeStandart;
            }
            else if (colorSchemeSwitcher.SelectedItem == ColorScheme.colorSchemeDark.name)
            {
                ActiveColorScheme = ColorScheme.colorSchemeDark;
            }
            else if (colorSchemeSwitcher.SelectedItem == ColorScheme.colorSchemeYellow.name)
            {
                ActiveColorScheme = ColorScheme.colorSchemeYellow;
            }
            // Сохраняем, для восстановления после перезапуска.
            Settings.Default.ColorSchemeIndex = ActiveColorScheme.Id;
            Settings.Default.Save();
        }

        // Обработчик события закрытия формы.
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Нужно пройтись по всем открытым документам,
            // И сохранить каждую вкладку.
            // При закрытии файл будет сохраняться (если он был до этого сохранен)
            // Или спрашивать у пользователя путь для сохранения.
            for (int i = 0; i < tabControl.TabCount; i++)
            {
                tabControl.SelectedIndex = i;
                CloseCurrentTabButton_Click(sender, e);
            }
            // Сохраняем настройки для следующего запуска.
            Settings.Default.Save();
            
        }

        // Обработчик события нажатия на клавиатуру.
        private void Notepad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.S && e.Shift)
            {
                // То же самое, что и сохранение всех документов по таймеру.
                AutoSaveTimer_Tick(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.S)
            {
                SaveButton_Click(sender, e);
            }
            else if (e.Alt && e.Shift && e.KeyCode == Keys.F4)
            {
                this.Close();
            }
            else if (e.Control && e.KeyCode == Keys.N)
            {
                CreateNewFileButton_Click(sender, e);
            }
            else if (e.Control && e.Shift && e.KeyCode == Keys.N)
            {
                CreateNewFileNewWindowButton_Click(sender, e);
            }
            // Отмена отмены (вперед).
            else if (e.Control && e.Shift && e.KeyCode == Keys.Z)
            {
                // Вызывем дважды, тк сначала сработает автоматический
                // CTRL+Z для textBox.
                activeTextBox.Redo();
                activeTextBox.Redo();
            }
            else if (e.Control && e.KeyCode == Keys.Z)
            {
                // Возращаем автоматическую отмену.
                activeTextBox.Redo();
                activeTextBox.Undo();
            }
        }
        
        // Обработчик события изменения интервала автосохранения.
        private void AutoSavePeriodChanger_ValueChanged(object sender, EventArgs e)
        {
            autoSaveTimer.Interval = (int)AutoSavePeriodChanger.Value * 1000;
            Settings.Default.AutoSavePeriod = (int)AutoSavePeriodChanger.Value;
            autoSaveTimer.Enabled = true;
        }

        // Обработчик события изменения интервала журналирования.
        private void SaveHistoryIntervalChanger_ValueChanged(object sender, EventArgs e)
        {
            saveHistoryTimer.Interval = (int)SaveHistoryIntervalChanger.Value * 1000;
            Settings.Default.SaveHistoryInterval = (int)SaveHistoryIntervalChanger.Value;
            saveHistoryTimer.Enabled = true;
        }
    }

    
    public class StringEventArgs : EventArgs
    {
        public string FileName;
    }
}
