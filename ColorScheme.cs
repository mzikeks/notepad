﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Notepad_
{
    // Класс, отвецающий за хранение цветов для цветового оформления приложения.
    public class ColorScheme
    {
        public ColorScheme() { }
        // Цвета в теме.
        public Color mainColor;
        public Color textColor;
        public string name;

        public int Id 
        {
            get;
            private set;
        }


        // Объявляем доступные темы.
        public static readonly ColorScheme colorSchemeStandart = new ColorScheme
        {
            mainColor = Color.White,
            textColor = Color.Black,
            Id = 0,
            name = "стандартная"
        };

        public static readonly ColorScheme colorSchemeYellow = new ColorScheme
        {
            mainColor = Color.LightYellow,
            textColor = ColorTranslator.FromHtml("#7b4e00"),
            Id = 1,
            name = "желтая"
        };

        public static readonly ColorScheme colorSchemeDark = new ColorScheme
        {
            mainColor = ColorTranslator.FromHtml("#7e97c0"),
            textColor = ColorTranslator.FromHtml("#1e134e"),
            Id = 2,
            name = "темная"
        };

        // Для получения темы из Settings.
        public static string GetColorSchemeNameById(int id)
        {
            if (id == 1) return colorSchemeYellow.name;
            else if (id == 2) return colorSchemeDark.name;
            return colorSchemeStandart.name;
        }
    }

    
}
