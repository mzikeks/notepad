﻿namespace Notepad_
{
    partial class Notepad
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MenuControl = new System.Windows.Forms.TabControl();
            this.FilePage = new System.Windows.Forms.TabPage();
            this.helpButton = new System.Windows.Forms.Button();
            this.CreateNewFileNewWindow = new System.Windows.Forms.Button();
            this.SaveAllDocsButton = new System.Windows.Forms.Button();
            this.CloseCurrentTabButton = new System.Windows.Forms.Button();
            this.CreateNewFileButton = new System.Windows.Forms.Button();
            this.OpenNewTabButton = new System.Windows.Forms.Button();
            this.saveAsButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.EditPage = new System.Windows.Forms.TabPage();
            this.RepeatButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.CutButton = new System.Windows.Forms.Button();
            this.PasteButton = new System.Windows.Forms.Button();
            this.SelectAllButton = new System.Windows.Forms.Button();
            this.CopyButton = new System.Windows.Forms.Button();
            this.FormatPage = new System.Windows.Forms.TabPage();
            this.formatControl = new Notepad_.FormatControl();
            this.SettingsPage = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.SaveHistoryIntervalChanger = new System.Windows.Forms.NumericUpDown();
            this.AutoSavePeriodChanger = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colorSchemeSwitcher = new System.Windows.Forms.ComboBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.MenuControl.SuspendLayout();
            this.FilePage.SuspendLayout();
            this.EditPage.SuspendLayout();
            this.FormatPage.SuspendLayout();
            this.SettingsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SaveHistoryIntervalChanger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoSavePeriodChanger)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.MenuControl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabControl, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(982, 551);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // MenuControl
            // 
            this.MenuControl.Controls.Add(this.FilePage);
            this.MenuControl.Controls.Add(this.EditPage);
            this.MenuControl.Controls.Add(this.FormatPage);
            this.MenuControl.Controls.Add(this.SettingsPage);
            this.MenuControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.MenuControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.MenuControl.Location = new System.Drawing.Point(3, 3);
            this.MenuControl.Name = "MenuControl";
            this.MenuControl.SelectedIndex = 3;
            this.MenuControl.Size = new System.Drawing.Size(976, 94);
            this.MenuControl.TabIndex = 0;
            // 
            // FilePage
            // 
            this.FilePage.BackColor = System.Drawing.Color.Transparent;
            this.FilePage.Controls.Add(this.helpButton);
            this.FilePage.Controls.Add(this.CreateNewFileNewWindow);
            this.FilePage.Controls.Add(this.SaveAllDocsButton);
            this.FilePage.Controls.Add(this.CloseCurrentTabButton);
            this.FilePage.Controls.Add(this.CreateNewFileButton);
            this.FilePage.Controls.Add(this.OpenNewTabButton);
            this.FilePage.Controls.Add(this.saveAsButton);
            this.FilePage.Controls.Add(this.saveButton);
            this.FilePage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FilePage.Location = new System.Drawing.Point(4, 29);
            this.FilePage.Margin = new System.Windows.Forms.Padding(0);
            this.FilePage.Name = "FilePage";
            this.FilePage.Size = new System.Drawing.Size(968, 61);
            this.FilePage.TabIndex = 0;
            this.FilePage.Text = "Файл";
            // 
            // helpButton
            // 
            this.helpButton.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.helpButton.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.helpButton.Location = new System.Drawing.Point(915, 5);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(43, 52);
            this.helpButton.TabIndex = 2;
            this.helpButton.Text = "?";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // CreateNewFileNewWindow
            // 
            this.CreateNewFileNewWindow.Location = new System.Drawing.Point(599, 5);
            this.CreateNewFileNewWindow.Name = "CreateNewFileNewWindow";
            this.CreateNewFileNewWindow.Size = new System.Drawing.Size(169, 52);
            this.CreateNewFileNewWindow.TabIndex = 0;
            this.CreateNewFileNewWindow.Text = "Создать новый файл в новом окне";
            this.CreateNewFileNewWindow.UseVisualStyleBackColor = true;
            this.CreateNewFileNewWindow.Click += new System.EventHandler(this.CreateNewFileNewWindowButton_Click);
            // 
            // SaveAllDocsButton
            // 
            this.SaveAllDocsButton.Location = new System.Drawing.Point(205, 5);
            this.SaveAllDocsButton.Name = "SaveAllDocsButton";
            this.SaveAllDocsButton.Size = new System.Drawing.Size(108, 52);
            this.SaveAllDocsButton.TabIndex = 1;
            this.SaveAllDocsButton.Text = "Сохранить все файлы";
            this.SaveAllDocsButton.UseVisualStyleBackColor = true;
            this.SaveAllDocsButton.Click += new System.EventHandler(this.AutoSaveTimer_Tick);
            // 
            // CloseCurrentTabButton
            // 
            this.CloseCurrentTabButton.Location = new System.Drawing.Point(774, 5);
            this.CloseCurrentTabButton.Name = "CloseCurrentTabButton";
            this.CloseCurrentTabButton.Size = new System.Drawing.Size(136, 52);
            this.CloseCurrentTabButton.TabIndex = 0;
            this.CloseCurrentTabButton.Text = "Закрыть текущий документ";
            this.CloseCurrentTabButton.UseVisualStyleBackColor = true;
            this.CloseCurrentTabButton.Click += new System.EventHandler(this.CloseCurrentTabButton_Click);
            // 
            // CreateNewFileButton
            // 
            this.CreateNewFileButton.Location = new System.Drawing.Point(459, 5);
            this.CreateNewFileButton.Name = "CreateNewFileButton";
            this.CreateNewFileButton.Size = new System.Drawing.Size(134, 52);
            this.CreateNewFileButton.TabIndex = 0;
            this.CreateNewFileButton.Text = "Создать новый файл";
            this.CreateNewFileButton.UseVisualStyleBackColor = true;
            this.CreateNewFileButton.Click += new System.EventHandler(this.CreateNewFileButton_Click);
            // 
            // OpenNewTabButton
            // 
            this.OpenNewTabButton.Location = new System.Drawing.Point(319, 5);
            this.OpenNewTabButton.Name = "OpenNewTabButton";
            this.OpenNewTabButton.Size = new System.Drawing.Size(134, 52);
            this.OpenNewTabButton.TabIndex = 0;
            this.OpenNewTabButton.Text = "Открыть в новой вкладке";
            this.OpenNewTabButton.UseVisualStyleBackColor = true;
            this.OpenNewTabButton.Click += new System.EventHandler(this.OpenNewTabButton_Click);
            // 
            // saveAsButton
            // 
            this.saveAsButton.Location = new System.Drawing.Point(105, 5);
            this.saveAsButton.Name = "saveAsButton";
            this.saveAsButton.Size = new System.Drawing.Size(94, 52);
            this.saveAsButton.TabIndex = 0;
            this.saveAsButton.Text = "Сохранить как";
            this.saveAsButton.UseVisualStyleBackColor = true;
            this.saveAsButton.Click += new System.EventHandler(this.SaveAsButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(5, 5);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(94, 52);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "&Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // EditPage
            // 
            this.EditPage.Controls.Add(this.RepeatButton);
            this.EditPage.Controls.Add(this.CancelButton);
            this.EditPage.Controls.Add(this.CutButton);
            this.EditPage.Controls.Add(this.PasteButton);
            this.EditPage.Controls.Add(this.SelectAllButton);
            this.EditPage.Controls.Add(this.CopyButton);
            this.EditPage.Location = new System.Drawing.Point(4, 29);
            this.EditPage.Name = "EditPage";
            this.EditPage.Padding = new System.Windows.Forms.Padding(3);
            this.EditPage.Size = new System.Drawing.Size(968, 61);
            this.EditPage.TabIndex = 1;
            this.EditPage.Text = "Правка";
            this.EditPage.UseVisualStyleBackColor = true;
            // 
            // RepeatButton
            // 
            this.RepeatButton.Location = new System.Drawing.Point(668, 10);
            this.RepeatButton.Name = "RepeatButton";
            this.RepeatButton.Size = new System.Drawing.Size(110, 38);
            this.RepeatButton.TabIndex = 1;
            this.RepeatButton.Text = "Вперед";
            this.RepeatButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(539, 10);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(110, 38);
            this.CancelButton.TabIndex = 1;
            this.CancelButton.Text = "Назад";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // CutButton
            // 
            this.CutButton.Location = new System.Drawing.Point(147, 10);
            this.CutButton.Name = "CutButton";
            this.CutButton.Size = new System.Drawing.Size(115, 38);
            this.CutButton.TabIndex = 0;
            this.CutButton.Text = "Вырезать";
            this.CutButton.UseVisualStyleBackColor = true;
            // 
            // PasteButton
            // 
            this.PasteButton.Location = new System.Drawing.Point(277, 10);
            this.PasteButton.Name = "PasteButton";
            this.PasteButton.Size = new System.Drawing.Size(115, 38);
            this.PasteButton.TabIndex = 0;
            this.PasteButton.Text = "Вставить";
            this.PasteButton.UseVisualStyleBackColor = true;
            // 
            // SelectAllButton
            // 
            this.SelectAllButton.Location = new System.Drawing.Point(408, 10);
            this.SelectAllButton.Name = "SelectAllButton";
            this.SelectAllButton.Size = new System.Drawing.Size(115, 38);
            this.SelectAllButton.TabIndex = 0;
            this.SelectAllButton.Text = "Выбрать всё";
            this.SelectAllButton.UseVisualStyleBackColor = true;
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(16, 10);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(115, 38);
            this.CopyButton.TabIndex = 0;
            this.CopyButton.Text = "Копировать";
            this.CopyButton.UseVisualStyleBackColor = true;
            // 
            // FormatPage
            // 
            this.FormatPage.Controls.Add(this.formatControl);
            this.FormatPage.Location = new System.Drawing.Point(4, 29);
            this.FormatPage.Name = "FormatPage";
            this.FormatPage.Padding = new System.Windows.Forms.Padding(3);
            this.FormatPage.Size = new System.Drawing.Size(968, 61);
            this.FormatPage.TabIndex = 2;
            this.FormatPage.Text = "Формат";
            this.FormatPage.UseVisualStyleBackColor = true;
            // 
            // formatControl
            // 
            this.formatControl.Location = new System.Drawing.Point(0, 3);
            this.formatControl.Name = "formatControl";
            this.formatControl.Size = new System.Drawing.Size(621, 39);
            this.formatControl.TabIndex = 0;
            // 
            // SettingsPage
            // 
            this.SettingsPage.Controls.Add(this.label3);
            this.SettingsPage.Controls.Add(this.SaveHistoryIntervalChanger);
            this.SettingsPage.Controls.Add(this.AutoSavePeriodChanger);
            this.SettingsPage.Controls.Add(this.label2);
            this.SettingsPage.Controls.Add(this.label1);
            this.SettingsPage.Controls.Add(this.colorSchemeSwitcher);
            this.SettingsPage.Location = new System.Drawing.Point(4, 29);
            this.SettingsPage.Name = "SettingsPage";
            this.SettingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.SettingsPage.Size = new System.Drawing.Size(968, 61);
            this.SettingsPage.TabIndex = 3;
            this.SettingsPage.Text = "Настройки";
            this.SettingsPage.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(261, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Интервал журналирования (секунд)";
            // 
            // SaveHistoryIntervalChanger
            // 
            this.SaveHistoryIntervalChanger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SaveHistoryIntervalChanger.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.SaveHistoryIntervalChanger.Location = new System.Drawing.Point(272, 6);
            this.SaveHistoryIntervalChanger.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SaveHistoryIntervalChanger.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.SaveHistoryIntervalChanger.Name = "SaveHistoryIntervalChanger";
            this.SaveHistoryIntervalChanger.Size = new System.Drawing.Size(66, 27);
            this.SaveHistoryIntervalChanger.TabIndex = 3;
            this.SaveHistoryIntervalChanger.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.SaveHistoryIntervalChanger.ValueChanged += new System.EventHandler(this.SaveHistoryIntervalChanger_ValueChanged);
            // 
            // AutoSavePeriodChanger
            // 
            this.AutoSavePeriodChanger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AutoSavePeriodChanger.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AutoSavePeriodChanger.Location = new System.Drawing.Point(252, 34);
            this.AutoSavePeriodChanger.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.AutoSavePeriodChanger.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AutoSavePeriodChanger.Name = "AutoSavePeriodChanger";
            this.AutoSavePeriodChanger.Size = new System.Drawing.Size(66, 27);
            this.AutoSavePeriodChanger.TabIndex = 3;
            this.AutoSavePeriodChanger.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.AutoSavePeriodChanger.ValueChanged += new System.EventHandler(this.AutoSavePeriodChanger_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Автосохранение каждые (секунд)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(357, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Цветовая схема:";
            // 
            // colorSchemeSwitcher
            // 
            this.colorSchemeSwitcher.FormattingEnabled = true;
            this.colorSchemeSwitcher.Items.AddRange(new object[] {
            "темная",
            "стандартная",
            "желтая"});
            this.colorSchemeSwitcher.Location = new System.Drawing.Point(483, 5);
            this.colorSchemeSwitcher.Name = "colorSchemeSwitcher";
            this.colorSchemeSwitcher.Size = new System.Drawing.Size(151, 28);
            this.colorSchemeSwitcher.TabIndex = 0;
            this.colorSchemeSwitcher.Tag = "";
            this.colorSchemeSwitcher.SelectedIndexChanged += new System.EventHandler(this.colorSchemeSwitcher_SelectedIndexChanged);
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(3, 103);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(976, 445);
            this.tabControl.TabIndex = 1;
            // 
            // Notepad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(982, 551);
            this.Controls.Add(this.tableLayoutPanel1);
            this.HelpButton = true;
            this.KeyPreview = true;
            this.Name = "Notepad";
            this.Text = "Notepad+";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Notepad_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.MenuControl.ResumeLayout(false);
            this.FilePage.ResumeLayout(false);
            this.EditPage.ResumeLayout(false);
            this.FormatPage.ResumeLayout(false);
            this.SettingsPage.ResumeLayout(false);
            this.SettingsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SaveHistoryIntervalChanger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoSavePeriodChanger)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl MenuControl;
        private System.Windows.Forms.TabPage FilePage;
        private System.Windows.Forms.TabPage EditPage;
        private System.Windows.Forms.TabPage FormatPage;
        private System.Windows.Forms.TabPage SettingsPage;
        private FormatControl formatControl;
        private System.Windows.Forms.Button OpenNewTabButton;
        private System.Windows.Forms.Button saveAsButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button CreateNewFileButton;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Button CloseCurrentTabButton;
        private System.Windows.Forms.ComboBox colorSchemeSwitcher;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown AutoSavePeriodChanger;
        private System.Windows.Forms.Button CreateNewFileNewWindow;
        private System.Windows.Forms.Button SaveAllDocsButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Button CutButton;
        private System.Windows.Forms.Button PasteButton;
        private System.Windows.Forms.Button SelectAllButton;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown SaveHistoryIntervalChanger;
        private System.Windows.Forms.Button RepeatButton;
        private System.Windows.Forms.Button CancelButton;
    }
}

