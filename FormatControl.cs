﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Notepad_
{
    public partial class FormatControl : UserControl
    {

        public event EventHandler StyleChangedEvent;

        public FormatControl()
        {
            InitializeComponent();
            boldControl.CheckedChanged += (object s, EventArgs e) => StyleChangedEvent(s, e);
            italicControl.CheckedChanged += (object s, EventArgs e) => StyleChangedEvent(s, e);
            underlineControl.CheckedChanged += (object s, EventArgs e) => StyleChangedEvent(s, e);
            strikeoutControl.CheckedChanged += (object s, EventArgs e) => StyleChangedEvent(s, e);

        }
    }
}
