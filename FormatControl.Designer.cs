﻿namespace Notepad_
{
    partial class FormatControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boldControl = new System.Windows.Forms.CheckBox();
            this.italicControl = new System.Windows.Forms.CheckBox();
            this.underlineControl = new System.Windows.Forms.CheckBox();
            this.strikeoutControl = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // boldControl
            // 
            this.boldControl.AutoSize = true;
            this.boldControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.boldControl.Location = new System.Drawing.Point(0, 0);
            this.boldControl.Name = "boldControl";
            this.boldControl.Size = new System.Drawing.Size(92, 24);
            this.boldControl.TabIndex = 0;
            this.boldControl.Text = "жирный";
            this.boldControl.UseVisualStyleBackColor = true;
            // 
            // italicControl
            // 
            this.italicControl.AutoSize = true;
            this.italicControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.italicControl.Location = new System.Drawing.Point(98, 0);
            this.italicControl.Name = "italicControl";
            this.italicControl.Size = new System.Drawing.Size(76, 24);
            this.italicControl.TabIndex = 1;
            this.italicControl.Text = "курсив";
            this.italicControl.UseVisualStyleBackColor = true;
            // 
            // underlineControl
            // 
            this.underlineControl.AutoSize = true;
            this.underlineControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.underlineControl.Location = new System.Drawing.Point(180, 0);
            this.underlineControl.Name = "underlineControl";
            this.underlineControl.Size = new System.Drawing.Size(133, 24);
            this.underlineControl.TabIndex = 2;
            this.underlineControl.Text = "Подчеркнутый";
            this.underlineControl.UseVisualStyleBackColor = true;
            // 
            // strikeoutControl
            // 
            this.strikeoutControl.AutoSize = true;
            this.strikeoutControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point);
            this.strikeoutControl.Location = new System.Drawing.Point(319, 0);
            this.strikeoutControl.Name = "strikeoutControl";
            this.strikeoutControl.Size = new System.Drawing.Size(120, 24);
            this.strikeoutControl.TabIndex = 3;
            this.strikeoutControl.Text = "зачеркнутый";
            this.strikeoutControl.UseVisualStyleBackColor = true;
            // 
            // FormatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.strikeoutControl);
            this.Controls.Add(this.underlineControl);
            this.Controls.Add(this.italicControl);
            this.Controls.Add(this.boldControl);
            this.Name = "FormatControl";
            this.Size = new System.Drawing.Size(438, 23);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.CheckBox boldControl;
        internal System.Windows.Forms.CheckBox italicControl;
        internal System.Windows.Forms.CheckBox underlineControl;
        internal System.Windows.Forms.CheckBox strikeoutControl;
    }
}
